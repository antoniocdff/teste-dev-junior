# Desafio teste Dev Junior

---- Objetivo 

Criar um programa com frontend em Vuejs que importe a planilha de compras anexa e criar as telas de CRUD para editar as informações da planilha. 

---- Funcionalidades esperadas:

- Migrar a planilha xls podendo incluir diversos itens simultâneos.
- O campo OFERTA deverá ser único na tabela, avisando ao usuário caso tente importar um nome repetito
- O campo CLIENTE deverá ser uma chave estrangeira, validando a migração pelo nome da pessoa e caso não exista informar ao usuário.
- O CRUD deverá ter pesquisa, paginação, e formulário com possibilidade de alterar todos os campos.
- A função excluir deverá colocar o registro Ativo = False
- O campo CLIENTE no formulário deverá ser uma caixa de seleção com preenchimento automático (autocomplete)


---- Detalhes da entrega:
O código-fonte do projeto deverá ser bem documentado, incluindo a forma de implantação (deploy), deverá estar hospedado no github (ou gitlab), colocando a permissão para que eu possar clonar (email: antoniocarlosdff@gmail.com).

---- Stacks Obrigatórias:
- Vue3 + VueX + Vitte

---- Obs:
O backend e o banco de dados pode ser a critério do desenvolvedor porém é necessário comunicação via api rest
